package com.experis.movie_character_java.data_access;

import com.experis.movie_character_java.models.Track;

import java.util.ArrayList;

public interface TrackRepository {
    ArrayList<String> select(String table, int limit);
    Track selectTrack(String trackName);
}
