package com.experis.movie_character_java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieCharacterJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieCharacterJavaApplication.class, args);
	}

}
