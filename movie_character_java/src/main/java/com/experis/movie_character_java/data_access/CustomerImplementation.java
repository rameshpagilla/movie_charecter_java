package com.experis.movie_character_java.data_access;

import com.experis.movie_character_java.logging.LogToConsole;
import com.experis.movie_character_java.models.Customer;
import com.experis.movie_character_java.models.CustomerByCountry;
import com.experis.movie_character_java.models.CustomerGenre;
import com.experis.movie_character_java.models.CustomerSpender;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
@Repository
public class CustomerImplementation implements CustomerRepository{
    private final LogToConsole logger;
    private String URL=ConnectionHelper.CONNECTION_URL;
    private Connection conn=null;

    public CustomerImplementation(LogToConsole logger) {
        this.logger = logger;
    }

    //performs simple SELECT query on customer table
    @Override
    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customers=new ArrayList<>();
        try{
            conn= DriverManager.getConnection(URL);
            logger.log("Connection established");
            PreparedStatement preparedStatement=
                    conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,Email,Phone,PostalCode FROM customer");
            ResultSet resultSet=preparedStatement.executeQuery();
            while(resultSet.next()){
                customers.add(
                        new Customer(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("Email"),
                                resultSet.getString("Phone"),
                                resultSet.getInt("PostalCode")

                        )
                );
            }
            logger.log("Successfully selected all customers");

        } catch (Exception exception) {
            logger.log(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
logger.log(exception.toString());            }
        }
        return customers;
    }

    //Select customer by Id
    @Override
    public Customer getCustomerById(String customerId) {
        Customer customer=null;
        try{
            //Connecting to DB
            conn= DriverManager.getConnection(URL);
            logger.log("Connection established"); //This message will be displayed in console if connection is success
            //Writing SQL query
            PreparedStatement preparedStatement=
                    conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,Email,Phone,PostalCode FROM customer WHERE CustomerId=?");
           preparedStatement.setString(1,customerId);
            ResultSet resultSet=preparedStatement.executeQuery();
            while(resultSet.next()){
             customer=new Customer(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("Email"),
                                resultSet.getString("Phone"),
                                resultSet.getInt("PostalCode")

                );
            }
            logger.log("Successfully selected customer with Given Id");


        } catch (Exception exception) {
            logger.log(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                logger.log(exception.toString());            }
        }
        return customer;
    }

//Search customer by Name
    @Override
    public ArrayList<Customer> getCustomerByName(String name) {
        ArrayList<Customer> customers=new ArrayList<>();
        try{
            conn= DriverManager.getConnection(URL);
            logger.log("Connection established");
            PreparedStatement preparedStatement=
                    conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,Email,Phone,PostalCode FROM customer  WHERE FirstName LIKE ? OR LastName LIKE ?");
            preparedStatement.setString(1, "%" + name + "%");
            preparedStatement.setString(2, "%" + name + "%");
            for(int i = 1; i <= preparedStatement.getParameterMetaData().getParameterCount(); i++)
                preparedStatement.setString(i, "%" + name + "%");
            ResultSet resultSet=preparedStatement.executeQuery();
            while(resultSet.next()){
                customers.add(
                        new Customer(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("Email"),
                                resultSet.getString("Phone"),
                                resultSet.getInt("PostalCode")

                        )
                );
            }
            logger.log("Successfully selected all customers with Like Name");

        } catch (Exception exception) {
            logger.log(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                logger.log(exception.toString());            }
        }
        return customers;
    }
//Prints customers with limit and offset params
    @Override
    public ArrayList<Customer> getSubsetCustomers(int limit, int offset) {
        ArrayList<Customer> customers=new ArrayList<>();
        try{
            conn= DriverManager.getConnection(URL);
            logger.log("Connection established");
            PreparedStatement preparedStatement=
                    conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,Email,Phone,PostalCode FROM customer LIMIT ?,?");
            preparedStatement.setInt(1, offset);
            preparedStatement.setInt(2, limit);
            ResultSet resultSet=preparedStatement.executeQuery();
            while(resultSet.next()){
                customers.add(
                        new Customer(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("Email"),
                                resultSet.getString("Phone"),
                                resultSet.getInt("PostalCode")

                        )
                );
            }
            logger.log("Successfully selected all customers");

        } catch (Exception exception) {
            logger.log(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                logger.log(exception.toString());            }
        }
        return customers;
    }
//Adds new customer
    @Override
    public Boolean addCustomer(Customer customer) {
        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection established");
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO Customer (CustomerId,FirstName,LastName, Country, PostalCode, Phone, Email) VALUES (?,?,?,?,?,?,?)");
            preparedStatement.setString(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setInt(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhoneNumber());
            preparedStatement.setString(7, customer.getEmail());
            preparedStatement.executeUpdate();
            return true;

        } catch (Exception exception) {
            logger.log(exception.toString());
            return false;
        } finally {
            try {
                conn.close();
            }
            catch (Exception exception)
            {
                logger.log(exception.toString());
            }
        }

    }
//Updating Customer
    @Override
    public Boolean updateCustomer(String id,Customer customer) {
        try {
            conn= DriverManager.getConnection(URL);

            logger.log("Connection to SQLite has been established.");

            PreparedStatement preparedStatement =
                    conn.prepareStatement("UPDATE customer SET FirstName = ? ,LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? WHERE CustomerId = ?");
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setInt(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhoneNumber());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setString(7, id);
            preparedStatement.executeUpdate();

            logger.log("Customer updated");
            return true;

        } catch (Exception exception) {
            logger.log(exception.toString());
            return false;
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                logger.log(exception.toString());
            }
        }
    }
//gets customer by Country Descending Order
    @Override
    public ArrayList<CustomerByCountry> getCustomersOrdersByCountry() {
        ArrayList<CustomerByCountry> countryCustomers = new ArrayList<>();
        try  {
            conn= DriverManager.getConnection(URL);
            logger.log("Connection established");
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT COUNT(CustomerId), Country FROM customer GROUP BY Country ORDER BY COUNT(CustomerId) DESC");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next())
                countryCustomers.add(new CustomerByCountry(resultSet.getInt("COUNT(CustomerId)"), resultSet.getString("Country")));

        }catch (Exception exception) {
            logger.log(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                logger.log(exception.toString());
            }
        }
        return countryCustomers;
    }

    //Performs an INNER JOIN on Invoice from com.experisacademy.model.Customer to allow for a com.experisacademy.model.Customer object ArrayList to be sorted by Invoice.total, descending
    //Customers are added to Arraylist customers which is then returned.

    public ArrayList<CustomerSpender> getCustomersHighestSpenders() {
        ArrayList<CustomerSpender> customers = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(URL)) {
            logger.log("Connection to SQLite has been established.");

            PreparedStatement preparedStatement =
                    conn.prepareStatement("""
                            SELECT customer.CustomerId , customer.FirstName,Invoice.Total
                            FROM Customer INNER JOIN Invoice  on customer.CustomerId = invoice.CustomerId
                            ORDER BY Invoice.Total DESC""");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new CustomerSpender(
                                resultSet.getString("CustomerId"),
                                resultSet.getInt("Total"),
                                resultSet.getString("FirstName")
                        )
                );
            }

        } catch (Exception exception) {
            logger.log(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                logger.log(exception.toString());
            }
        }
        return customers;
    }

    //Performs several INNER JOINs to unify the database enabling for com.experisacademy.model.Customer data to be order by favourite Genre
    public CustomerGenre getCustomerPopularGenre(String customerId) {
        CustomerGenre customer = null;
        try  {
            Connection conn = DriverManager.getConnection(URL);
           logger.log("Connection to SQLite has been established.");

            PreparedStatement preparedStatement =
                    conn.prepareStatement("""
                            WITH y as ( SELECT Customer.CustomerId, Customer.FirstName,Customer.LastName, Genre.Name, COUNT( InvoiceLine.Quantity) myCount
                            FROM Customer
                            INNER JOIN Invoice on Customer.CustomerId = Invoice.CustomerId
                            INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId
                            INNER JOIN Track on InvoiceLine.TrackId = Track.TrackId
                            INNER JOIN Genre on Track.GenreId = Genre.GenreId WHERE Customer.CustomerId = ?
                            GROUP BY Genre.Name
                            ORDER BY myCount  DESC )
                            SELECT CustomerId, FirstName, LastName, Name, myCount FROM y WHERE (SELECT max(myCount) from y) = myCount""");
            preparedStatement.setString(1, customerId);

            ResultSet resultSet = preparedStatement.executeQuery();

            customer = new CustomerGenre(
                    resultSet.getString("customerid"),
                    resultSet.getString("firstname"),
                    resultSet.getString("lastname")
            );
            while(resultSet.next())
                customer.addGenre(resultSet.getString("Name"));

        }catch (Exception exception) {
            logger.log(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                logger.log(exception.toString());
            }
        }
        return customer;
    }
}
