package com.experis.movie_character_java.models;

public class CustomerSpender {
    private String customerId;
    private int spent;
    private String firstName;

    public CustomerSpender(String customerId, int spent,String firstName) {
        this.customerId = customerId;
        this.spent = spent;
        this.firstName=firstName;

    }

    public String getCustomerId() {
        return customerId;
    }
public String getFirstName(){
        return firstName;
}
    public int getSpent() {
        return spent;
    }
}
