package com.experis.movie_character_java.controllers;

import com.experis.movie_character_java.data_access.TrackRepository;
import com.experis.movie_character_java.models.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ViewController {
    private static final String FIRST_TABLE = "Artist";
    private static final String SECOND_TABLE = "Track";
    private static final String THIRD_TABLE = "Genre";
    private static final int LIMIT = 6;

    private final TrackRepository trackRepository;
    @Autowired
    public ViewController(TrackRepository trackRepository) {
        this.trackRepository = trackRepository;
    }

    //GET a random set of artist-, song- and genre names. These are passed to a thymeleaf html page.
    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String home(Model model) {
        model.addAttribute("artists", trackRepository.select(FIRST_TABLE, LIMIT));
        model.addAttribute("songs", trackRepository.select(SECOND_TABLE, LIMIT));
        model.addAttribute("genres", trackRepository.select(THIRD_TABLE, LIMIT));
        return "home";
    }
    /* GET a Track object from the specified request parameter by passing it to
   the Repository layer. If the name exists in the database then this object is passed to a thymeleaf html page.
   Otherwise it returns to the home page.
   */
    @RequestMapping(value = "/results",method = RequestMethod.GET)
    public String displayResults(Model model, @RequestParam String name) {
        Track track = trackRepository.selectTrack(capitalizeEachWord(name));
        if (track != null) {
            model.addAttribute("success", true);
            model.addAttribute("track", track);
            return "results";
        }
        return home(model);
    }

    // Capitalizes first letter in each word and then returns the formatted sentence back.
    private String capitalizeEachWord(String sentence) {
        String formattedName = "";
        String words[] = sentence.trim().replaceAll(" +", " ").split(" ");
        for (String word : words) {
            formattedName += word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase() + " ";
        }
        return formattedName.trim();
    }

}
