package com.experis.movie_character_java.controllers;


import com.experis.movie_character_java.data_access.CustomerRepository;
import com.experis.movie_character_java.models.Customer;
import com.experis.movie_character_java.models.CustomerByCountry;
import com.experis.movie_character_java.models.CustomerGenre;
import com.experis.movie_character_java.models.CustomerSpender;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
@RestController
public class CustomerController {
  private final CustomerRepository customerRepository;
  public CustomerController(CustomerRepository customerRepository)
  {this.customerRepository=customerRepository;}

  //Gets all customers
    @RequestMapping(value="/api/customers",method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers()
    {
        return customerRepository.getAllCustomers();
    }
//Gets customer-details by given Id
  @RequestMapping(value="/api/customer/{id}",method = RequestMethod.GET)
  public Customer getCustomerById(@PathVariable String id)
  {
    return customerRepository.getCustomerById(id);
  }

  //Gets customer details by given name(prints all matching customers)
  @RequestMapping(value = "/api/customers/{firstName}", method = RequestMethod.GET)
  public ArrayList<Customer> searchByCustomerName(@PathVariable String firstName){
    return customerRepository.getCustomerByName(firstName);
  }

  //Requests a subset of customers based upon limit and offset
  @RequestMapping(value="/api/customers/{limit}/{offset}",method = RequestMethod.GET)
  public ArrayList<Customer> getSubsetCustomers(@PathVariable int limit,
                                                @PathVariable int offset) {
    return customerRepository.getSubsetCustomers(limit, offset);

  }
  //Inserts a new customer
  @RequestMapping(value = "/api/",method = RequestMethod.POST)
  public Boolean createCustomer(@RequestBody Customer customer){
    return customerRepository.addCustomer(customer);
  }

  //Edit customer By id
  @RequestMapping(value = "/api/customer/{id}",method = RequestMethod.PUT)
  public Boolean updateCustomer(@PathVariable String id, @RequestBody Customer customer ){
    return customerRepository.updateCustomer(id, customer);
  }

  //Requests the amount of customers registered to each country, then returns a list of those countries in descending order
  @RequestMapping(value="api/customers/country",method = RequestMethod.GET)
  public ArrayList<CustomerByCountry> getCustomersOrdersByCountry() {
    return customerRepository.getCustomersOrdersByCountry();
  }
  //Get high-spending customers
  @RequestMapping(value = "api/customers/spending",method = RequestMethod.GET)
  public ArrayList<CustomerSpender> getHighestSpenders() {
    return customerRepository.getCustomersHighestSpenders();
  }

  //Requests a specific customers favourite genre along with their normal details, by customer Id
  @RequestMapping(value = "api/customer/genre/{id}",method = RequestMethod.GET)
  public CustomerGenre getPopularGenreForGivenCustomerId(@PathVariable String id){
    return customerRepository.getCustomerPopularGenre(id);
  }
}
