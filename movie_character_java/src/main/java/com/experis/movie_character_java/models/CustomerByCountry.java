package com.experis.movie_character_java.models;

public class CustomerByCountry {
    private int count;
    private String Name;

    public CustomerByCountry(int count, String name) {
        this.count = count;
        Name = name;
    }

    public int getCount() {
        return count;
    }

    public String getName() {
        return Name;
    }
}
