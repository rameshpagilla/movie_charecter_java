package com.experis.movie_character_java.models;

import java.util.ArrayList;

public class CustomerGenre {

    private String customerId,firstName, lastName;
    private ArrayList<String> genres = new ArrayList<>();

    public CustomerGenre(String customerId, String firstName, String lastName) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public ArrayList<String> getGenres() {
        return genres;
    }
    public void addGenre(String genre) {
        genres.add(genre);
    }
}
