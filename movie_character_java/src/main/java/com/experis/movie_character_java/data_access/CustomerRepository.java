package com.experis.movie_character_java.data_access;

import com.experis.movie_character_java.models.Customer;
import com.experis.movie_character_java.models.CustomerByCountry;
import com.experis.movie_character_java.models.CustomerGenre;
import com.experis.movie_character_java.models.CustomerSpender;

import java.util.ArrayList;

public interface CustomerRepository {
    public ArrayList<Customer> getAllCustomers();
    public Customer getCustomerById(String customerId);
    public ArrayList<Customer> getCustomerByName(String name);
    ArrayList<Customer> getSubsetCustomers(int limit, int offset);
    public Boolean addCustomer(Customer customer);
    public Boolean updateCustomer(String id,Customer customer);
    public ArrayList<CustomerByCountry> getCustomersOrdersByCountry();
    public ArrayList<CustomerSpender> getCustomersHighestSpenders();
    public CustomerGenre getCustomerPopularGenre(String customerId);
}
